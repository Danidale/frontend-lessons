# Front End Lessons
## Lesson 1

This exercise is about creating a very simple static website.
Basic skills reqired are __HTML__ and __CSS__.

You will find an image called __Demo.png__ that shows how the final product must be – it’s like a super basic version of the Gucci website.
Inside the /media folder you'll find the very same images I used for the screenshot.

Focus on the html part first and on the box model.
Some CSS for colors, font size and family also will be necessary , but very basics things ;)
FrontEnd is something really detail-oriented so focus on the details.

As I said the website is static and has no needed action to be working.
The only thing that must work is that when I click on WOMAN in the menu the page scrolls to the Woman section below – and the same for MEN.

I recommend finding yourselves a good image editing software for things like measuring things and capturing colors.
Adobe Photoshop is suggested, but some free alternatives such as GIMP and Paint.NET could be a good starter.

Good work!
